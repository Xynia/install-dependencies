#!/bin/bash

: '
Quick description:
  This script can be used to install all required dependencies for a specific
  binary.
Last update: 24 sept 2019
'

echo "--------------------------- UPDATE PACKAGES ----------------------------"

apt-get update
apt-get install apt-file -y
apt-file update

echo "----------------------------- UPDATE DONE ------------------------------"

# Check if the given argument is correct
if [ "$#" -ne 1 ] || [ ! -x $1 ]
then
        RED='\033[0;31m'
        DEFAULT='\033[0m'
        echo -e "${RED}Argument error${DEFAULT}: this script take an executable file as argument"
        exit 0
fi

mkdir tmp_install_libs

if [ $? -eq 0 ]
then
        cd tmp_install_libs

        # Find all needed libraries and write result into all_lib.txt
        objdump -p ../$1 | grep NEEDED | cut -c 10- > all_lib.txt
        # Other way with ldd:
        #ldd ../$1 | cut -d '=' -f 1 | cut -d '/' -f 1 | cut -d '(' -f 1 \
        #        > all_lib.txt

        # Loop over every library
        while IFS= read -r line
        do
                # Skip if the line is blank only
                [[ $line =~ ^[[:blank:]]+$ ]] && continue

                # Check if the library is already installed or not
                ldconfig -p | grep $line > location.txt
                # If empty, the library has to be installed
                if [ ! -s ./location.txt ]
                then
                        echo "$line not found. It will be installed."
                        # Use the first package containing the library
                        pkg=$(apt-file --package-only find $line | head -n 1)
                        echo "package: $pkg"
                        # Install the package
                        apt-get install $pkg -y
                fi
        done < all_lib.txt

        cd ..
        rm -rf tmp_install_libs
fi
